#include"Linuxhost.h"
#include<thread>
#include<ros/ros.h>
#include"std_msgs/String.h"
#include"panda_msgs/PtbSendFile.h"
#define RECEIVE_PORT 5000
using namespace std;

void callback(const panda_msgs::PtbSendFile::ConstPtr& msg, Linuxhost& host){
	if (msg->type == "file"){
		struct Data data;
		data.local_path = msg->local_path;
		data.client_path = msg->client_path;
		host.send_data(msg->client_ip, msg->client_port, data, TYPE_FILE);
	}else if(msg->type == "message"){
		mymsg protomsg;

		//this part needs to be modified after the .proto is modified!!!
		protomsg.set_id(msg->id);
		protomsg.set_name(msg->name);
		protomsg.set_score(msg->score);

		string str_msg;
		protomsg.SerializeToString(&str_msg);

		struct Data data;
		data.info = str_msg;
		host.send_data(msg->client_ip, msg->client_port, data, TYPE_MESSAGE);
	}else{
		printf("Unknown type\n");
	}
	return;
}

int main(int argc,char** argv)
{
	Linuxhost host;
	//open the thread of receiving
	thread([&host] { host.receive_data(RECEIVE_PORT); }).detach();
	//subcribe the topic named chatter
	ros::init(argc,argv,"host");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe<panda_msgs::PtbSendFile>("chatter", 1000, boost::bind(&callback, _1, host));
	ros::spin();
    return 0;
}
