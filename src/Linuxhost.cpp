#include<Linuxhost.h>
using namespace std;

Linuxhost::Linuxhost(){
    if ((server_socket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Create Socket Failed!\n");
        exit(1);
    }
    setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
}

Linuxhost::~Linuxhost(){
    close(server_socket);
}

void Linuxhost::receive_data(int port){
    struct sockaddr_in server_addr;
    
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(port);
    
    if(bind(server_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)))
    {
        printf("Server Bind Port: %d Failed!\n", port);
        exit(1);
    }
    
    if (listen(server_socket, LENGTH_OF_LISTEN_QUEUE))
    {
        printf("Server Listen Failed!\n");
        exit(1);
    }else{
        printf("Listening...\n");
    }

    while(1)
    {
        struct sockaddr_in client_addr;
        int client_socket;
        socklen_t length;
        char buffer[BUFFER_SIZE];

        length = sizeof(client_addr);
        client_socket = accept(server_socket, (struct sockaddr*)&client_addr, &length);
        if (client_socket < 0)
        {
            printf("Server Accept Failed!\n");
            break;
        }
        //recv the filename and create it
        bzero(buffer, BUFFER_SIZE);
        length = recv(client_socket, buffer, BUFFER_SIZE, 0);
        buffer[length]=='\0';
        
        if (strcmp(buffer, "MSG")==0){
            char strbuf[MAXSTRBUF];
            bzero(strbuf, MAXSTRBUF);
            int p = 0;
            while ((length = recv(client_socket, strbuf+p, BUFFER_SIZE, 0)) > 0)
            {
                p += length;
            }
            string str_msg(strbuf, p);
            mymsg msg;
            msg.ParseFromString(str_msg);
            cout<<"ID: "<<msg.id()<<endl;
            cout<<"Name: "<<msg.name()<<endl;
            cout<<"Score: "<<msg.score()<<endl;
            //TODO: process msg received 
            
        }else if(strcmp(buffer,"NONE")==0){
            printf("Receive nothing.\n");
            //do nothing
        }else{
            printf("Receive file path is [%s]\n",buffer);
            FILE* f = fopen(buffer,"wb");
            while((length = recv(client_socket, buffer, BUFFER_SIZE, 0))>0)
            {
                fwrite(buffer,1,length,f);
            }
            fclose(f);
        }
        printf("Receive finish.\n");
        close(client_socket);
    }
}

void Linuxhost::send_data(string dst_ip, int dst_port, struct Data data, int data_type){
    struct sockaddr_in client_addr;
    int client_socket;

    if ((client_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        printf("Create Socket Failed!\n");
        exit(1);
    }
    
    //记录对方网络地址 
    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = inet_addr(dst_ip.c_str());
    client_addr.sin_port = htons(dst_port);
    memset(client_addr.sin_zero, 0X00, 8);

    //socket与对方地址连接 
    if (connect(client_socket, (struct sockaddr*)&client_addr, sizeof(client_addr)) < 0){
        printf("Socket Connect Error!\n");
        exit(0);
    }else{
        printf("Socket Connect Succeed!\n");
    }
    //main task 
    if (data_type == TYPE_FILE){
        send_file(client_socket, data.local_path, data.client_path);
    }else{
        send_message(client_socket, data.info);
    }
    
    //关闭socket
    close(client_socket);
}

void Linuxhost::send_file(int csocket, string local_path, string client_path){
    printf("Local path is: [%s]\n",local_path.c_str());
    printf("Remote path is: [%s]\n",client_path.c_str());
    
    FILE* f = fopen(local_path.c_str(),"rb");
    if(f == NULL){
        std::string tip = "NONE";
        send(csocket,tip.c_str(),(int)tip.length(),0);
        printf("File open error.\n");
        return;
    }
    //发送文件名 
    send(csocket, client_path.c_str(), (int)client_path.length(), 0);
    //avoid sending too fast
    sleep(1);
    //发送文件体 
    int nCount,sum = 0;
    char sendData[BUFFER_SIZE];
    
    while((nCount=fread(sendData, 1, BUFFER_SIZE, f))>0)
    {
        printf("%db\n",sum+=nCount);
        send(csocket, sendData, nCount, 0);
    }

    fclose(f);
    printf("Send File Success.\n");
    return;
}

void Linuxhost::send_message(int csocket, string str_msg){
    
    //send head, make receiver know its message
    string head = "MSG";
    send(csocket, head.c_str(), (int)head.length(), 0);

    sleep(1);
    //send string
    int p = 0;
    while(p<str_msg.length()){
        if(str_msg.length()-p >= BUFFER_SIZE){
            send(csocket, str_msg.c_str()+p, BUFFER_SIZE, 0);
            p += BUFFER_SIZE;
        }else{
            send(csocket, str_msg.c_str()+p, str_msg.length()-p, 0);
            p = str_msg.length();
        }
    }
    printf("Send Message Success.\n");
    return;
}
