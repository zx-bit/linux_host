#include"mymsg.pb.h"
#include<unistd.h>
#include<cstdio>
#include<iostream>
#include<x86_64-linux-gnu/sys/socket.h>
#include<netinet/in.h>
#include<x86_64-linux-gnu/sys/types.h>
#include<stdlib.h>
#include<cstring>
#include<string>
#include<arpa/inet.h>
#define TYPE_FILE 0
#define TYPE_MESSAGE 1
#define LENGTH_OF_LISTEN_QUEUE 20
#define BUFFER_SIZE 512
#define MAXSTRBUF 1000000
using namespace std;

struct Data
{
    string info="";
    string local_path="";
    string client_path="";
};

class Linuxhost
{
public:
    Linuxhost();
    ~Linuxhost();

    //receive the message or file
    void receive_data(int port);
    //send the message or file
    void send_data(string dst_ip, int dst_port, struct Data data, int data_type = TYPE_FILE);

private:
    int server_socket;
    int opt = 1;

    //following two functions are called by function 'send_data'
    void send_file(int csocket, string , string);
    void send_message(int csocket, string str_msg);
};
