# Linuxhost项目

## 一、功能

在Ubuntu下，使用TCP协议实现自定义格式数据和文件的发送与接收，程序运行进行了ros环境包装。

程序会接收一个message消息，该信息中还有目的端口、ip和发送文件名等相关信息；当接收到message消息后，程序会将服务器上的文件或特定格式消息发送给客户端。

## 二、环境要求

> Ubuntu18.4+ROS-melodic
>
> protobuf version：3.17.3
>
> cmake version：3.10.2

## 三、使用方法

#### 1.一般方法：

首先，配置好自己的ros_workstation文件夹；

将下载的来的文件夹命名为linux_host，并放在：your_ros_workstation_path/src下，该linux_host即作为一个ros node。之后执行如下操作，即可运行成功。

```shell
#使用ros编译器编译程序
cd your_ros_workstation_path
catkin_make

#运行roscore
roscore

#打开另一个terminal运行我们的程序
cd your_ros_workstation_path
source devel/setup.bash
rosrun linux_host linux_host_node
```

程序运行后，会订阅一个名为“chatter”的rostopic的PtbSendFile信息。我们只需要打开另一个terminal，使用rostopic执行如下命令，发布一个PtbSendFile信息，程序就会将相关的文件或消息发送到目的客户端。

```shell
rostopic pub -1 chatter linux_host/PtbSendFile "header:
  seq: 0
  stamp: {secs: 0, nsecs: 0}
  frame_id: ''
local_ip: ''	# 未用到
local_port: 0	# 未用到
local_path: ''	# 发送的文件位于服务器什么地方，如/usr/a.txt是发送usr下的文件
client_ip: ''	# 客户端ip地址
client_port: 5000 # 因为接收程序的端口是5000
client_path: ''	# 接收到的文件保存在客户端的什么地方，如D:\\a.txt是存在d盘下
file_sent: false # 未用到
type: ''		# message or file
name: ''		# 以下是发送message时，自定义的消息格式
id: 0
score: 0.0"
# 注意：
# 发送文件type设为file，发送消息type设为message
# 发送文件时，会将服务器的local_path的文件发送到对应client的client_path上
# 发送消息时，会将name，id，score经过protobuf打包后，发送到对应client上
```

需要注意的是，该程序的发送过程需要目的客户端有接收程序运行。因此，在发送前，需要先将客户端的接收程序打开。

Windows客户端的接收程序在下面项目中。

https://gitlab.com/zx-bit/win_host

Linux客户端的接收程序已包含在本项目中，只需要在客户端的机器上运行本程序，即可接收。**必须注意的是接收程序的接收端口是5000**！

#### 2.快速方法：

如果想快速使用该程序，请将linux_host项目放于~/catkin_ws/src下；

然后在linux_host/test文件夹下打开terminal，**先执行sh compile.sh**，该脚本完成了项目的编译；

**接着执行sh test.sh**，该脚本完成了roscore启动、程序的打开等。

最后，在脚本所打开的位于~/catkin_ws的terminal中，执行上面代码块中的rostopic过程即可进行发送。

注意：该方法在发送数据前，也需要将客户端的接收程序打开。

## 四、文件介绍

**protobuf**：mymsg.proto是自定义数据的格式文件，自定义数据的格式在该文件中修改；protoc是protobuf的编译器，功能是将.proto文件编译成.h和.cc文件。

```shell
#更改mymsg.proto格式后，在protobuf文件夹下使用terminal执行如下命令
./protoc -I=./ --cpp_out=./ ./mymsg.proto
#将新生成的.h和.cpp将旧的手动替换即可
```

**lib**：protobuf所需要的静态库。

**include/google**：protobuf所需的头文件。

**include/linux_host**：Linuxhost.h是我们编写的Linuxhost类的头文件，mymsg.pb.h是protoc编译器生成的可供我们调用的文件。

**src**：源文件。Linuxhost.cpp是Linuxhost.h的实现，linux_host_node.cpp实例化了Linuxhost类，mymsg.pb.cc是protoc编译器生成的可供我们调用的文件。

**msg**：存储自定义的消息的文件夹。PtbSendFile.msg为我们自定义的消息，包含目的端口、IP和发送文件等信息。

**test**：该文件夹存储了快速进行程序运行验证的脚本。compile.sh用于编译项目，test.sh用于与运行程序。
